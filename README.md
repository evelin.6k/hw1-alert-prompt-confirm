# HW1 - alert prompt confirm

1.  Let - объявление переменной, значение которой может быть изменено.
	Const - присваивание значения на постоянной основе, неизменяемое значение, константа.
	Var - устаревший способ объявления переменной. Синтаксис var схож с синтаксисом let.

2.  1) Переменные не имеют блочной области видимости. Область видимости - тело функции, (либо, если переменная глобальная, то 			скриптом). Это влияет на объявления внутри if, while или for.

	2) Объявления переменных var обрабатываются в начале выполнения функции (или запуска скрипта, если переменная является глобальной). Инициализация переменной остается в месте ее записи, но определление перемещается к верхушке функции.